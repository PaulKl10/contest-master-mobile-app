import Constants from 'expo-constants';

const IP = Constants.expoConfig?.extra?.IP_local;
const baseUrl:string = `http://${IP}:8048/api`;
export default baseUrl;