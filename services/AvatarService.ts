import apiClient from "./apiClient";

const getMyAvatars = async (currentUserId: number) => {
    try {
        const response = await apiClient.get(
            `/avatar/avatars/${currentUserId}`
        );
        return response.data;
    } catch (error) {
        console.error(error);
    }
};

const getAllAvatars = async () => {
    try {
        const response = await apiClient.get(`/avatar/`);
        return response.data;
    } catch (error) {
        console.log(error);
    }
};

const addAvatarToUser = async (currentUserId: number, avatarId: number) => {
    try {
        console.log(`salut`);
        const response = await apiClient.post(
            `/avatar/addToUser/${currentUserId}`,
            { avatarId },
            {
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );
        return response.data;
    } catch (error) {
        alert("Vous possédez déjà cet avatar");
        throw error;
    }
};

const AvatarService = {
    getMyAvatars,
    getAllAvatars,
    addAvatarToUser,
};

export default AvatarService;
