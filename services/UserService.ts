import { TUser } from "@/types/user.types";
import apiClient from "./apiClient";

const getAllUsers = async () => {
    try {
        const response = await apiClient.get(`/user/`);
        return response.data;
    } catch (error) {
        console.log(error);
    }
};

const getUserByUid = async (currentUserUid: string) => {
    try {
        const response = await apiClient.get<TUser>(
            `/user/uid/${currentUserUid}`
        );
        return response.data;
    } catch (error) {
        console.error(error);
    }
};

const UserService = {
    getAllUsers,
    getUserByUid,
};

export default UserService;
