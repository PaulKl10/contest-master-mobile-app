import AvatarService from "@/services/AvatarService";

const useGetAvatars = async () => {
    const avatars = await AvatarService.getAllAvatars();
    return avatars;
};

export default useGetAvatars;
