// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getReactNativePersistence, initializeAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import ReactNativeAsyncStorage from '@react-native-async-storage/async-storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDtqIvqFsx047PEHUBDazK-fRgkaWxafVo",
  authDomain: "contest-master.firebaseapp.com",
  projectId: "contest-master",
  storageBucket: "contest-master.appspot.com",
  messagingSenderId: "797797404996",
  appId: "1:797797404996:web:660314cde243f3d7b94711"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialiser Firebase Auth avec AsyncStorage
const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(ReactNativeAsyncStorage),
});
// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

export { auth, db };