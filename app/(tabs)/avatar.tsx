import { View } from "@/components/Themed";
import MyAvatars from "../../Views/MyAvatars";

export default function Avatar() {
    return (
        <View>
            <MyAvatars />
        </View>
    );
}
