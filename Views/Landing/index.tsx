import tw from "@/lib/tailwind";
import React from "react";
import { Pressable, Keyboard } from "react-native";
import Banner from "@/components/Banner";
import { StatusBar } from "expo-status-bar";
import LoginForm from "@/components/LoginForm";

export default function Landing() {
    return (
        <Pressable
            style={tw`flex-1 justify-center items-center bg-black gap-24`}
            onPress={Keyboard.dismiss}
        >
            <StatusBar style="light" />
            <Banner />
            <LoginForm />
        </Pressable>
    );
}
