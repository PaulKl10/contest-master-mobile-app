import { Text, TouchableOpacity, Image } from "react-native";
import React from "react";
import tw from "@/lib/tailwind";
import { TAvatar } from "@/types/avatar.types";

interface IAvatarListItemProps {
    avatar: TAvatar;
}

export default function AvatarListItem({ avatar }: IAvatarListItemProps) {
    return (
        <TouchableOpacity
            style={tw`p-2 border rounded-2 border-cm-yellow flex items-center justify-center gap-2`}
        >
            <Text style={tw`text-white`}>{avatar.name}</Text>
            <Image
                source={{
                    uri: avatar.image,
                }}
                width={62}
                height={62}
            />
        </TouchableOpacity>
    );
}
