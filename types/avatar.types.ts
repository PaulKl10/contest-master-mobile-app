export type TAvatar = {
    id: number;
    name: string;
    description: string;
    image: string;
    cost: number;
};
