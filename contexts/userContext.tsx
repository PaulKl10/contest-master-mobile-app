import useAuthentication from "@/hooks/useAuthentification";
import { auth } from "@/services/firebaseConfig";
import UserService from "@/services/UserService";
import { TUser } from "@/types/user.types";
import {
    createContext,
    FC,
    PropsWithChildren,
    useContext,
    useEffect,
    useMemo,
    useState,
} from "react";

interface IUserContextProps {
    user: TUser | null;
}

const UserContext = createContext<IUserContextProps>({ user: null });
UserContext.displayName = "UserContext";

export const UserProvider: FC<PropsWithChildren> = ({ children }) => {
    const [user, setUser] = useState<TUser | null>(null);

    const currentUserUid = useAuthentication(auth)?.uid;

    const value = useMemo(() => ({ user: user }), [user]);

    useEffect(() => {
        if (currentUserUid) {
            UserService.getUserByUid(currentUserUid).then((userFromBack) => {
                userFromBack ? setUser(userFromBack) : "";
            });
        }
    }, [currentUserUid]);

    return (
        <UserContext.Provider value={value}>{children}</UserContext.Provider>
    );
};
UserProvider.displayName = "UserProvider";

export const useUserContext = () => useContext(UserContext);
