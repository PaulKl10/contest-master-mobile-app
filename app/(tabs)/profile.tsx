import { Pressable, Image, ScrollView } from "react-native";
import tw from "@/lib/tailwind";
import { auth } from "@/services/firebaseConfig";
import { Text, View } from "@/components/Themed";
import { useRouter } from "expo-router";
import { useUserContext } from "@/contexts/userContext";

export default function Profile() {
    const nav = useRouter();
    const handleSignOut = async () => {
        auth.signOut().then(() => {
            alert("account disconnected");
            nav.dismissAll();
        });
    };
    const userContext = useUserContext();

    if (!userContext.user) {
        return (
            <ScrollView style={tw`bg-cm-black py-20`}>
                <Text style={tw`text-white`}>Error</Text>
            </ScrollView>
        );
    }

    return (
        <ScrollView style={tw`bg-cm-black py-20`}>
            <View style={tw`bg-cm-black flex-1 items-center`}>
                <Image
                    style={tw`w-30 h-30 rounded-full border border-cm-yellow`}
                    source={require("../../assets/images/user_profil.png")}
                />
                {/* Separator */}
                <View style={tw`my-[30px] h-[1px] w-80 bg-cm-yellow`} />
                {/* Description */}
                <Text style={tw`text-white`}>Nom: {userContext.user.name}</Text>
                <View style={tw`my-[30px] h-[1px] w-80 bg-cm-yellow`} />
                <Text style={tw`text-white`}>
                    Pseudo : {userContext.user.pseudo}
                </Text>
                <View style={tw`my-[30px] h-[1px] w-80 bg-cm-yellow`} />
                <Text style={tw`text-white`}>
                    Description: {userContext.user.description ?? ""}
                </Text>
                <View style={tw`my-[30px] h-[1px] w-80 bg-cm-yellow`} />
                <Text style={tw`text-white`}>
                    Ville: {userContext.user.city.name}
                </Text>
                <View style={tw`my-[30px] h-[1px] w-80 bg-cm-yellow`} />
                <Text style={tw`text-white`}>
                    Email: {userContext.user.email}
                </Text>
                <View style={tw`my-[30px] h-[1px] w-80 bg-cm-yellow`} />
                <Text style={tw`text-white`}>
                    Point: {userContext.user.gold ?? 0}
                </Text>
            </View>
            <Pressable style={tw`mx-auto py-40`} onPress={handleSignOut}>
                <Text
                    style={tw`text-2xl text-cm-yellow hover:text-white`}
                >{`Déconnexion`}</Text>
            </Pressable>
        </ScrollView>
    );
}
