import { View, FlatList } from "react-native";
import React from "react";
import AvatarListItem from "../AvatarListItem";
import tw from "@/lib/tailwind";
import { TAvatar } from "@/types/avatar.types";

interface IAvatarListProps {
    avatars: TAvatar[];
}

export default function AvatarList({ avatars }: IAvatarListProps) {
    return (
        <View style={tw`p-2 w-100`}>
            {avatars && (
                <FlatList
                    data={avatars}
                    renderItem={({ item }) => <AvatarListItem avatar={item} />}
                    keyExtractor={(item, index) =>
                        `${String(item.id)}+${index}`
                    }
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    ItemSeparatorComponent={() => <View style={tw`w-2`} />}
                />
            )}
        </View>
    );
}
