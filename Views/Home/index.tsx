import { Text, View } from '@/components/Themed';
import { Link } from 'expo-router';
import tw from '@/lib/tailwind';
import { SafeAreaView } from 'react-native-safe-area-context';
import Banner from "@/components/Banner";

export default function Home() {
  return (
    <SafeAreaView style={tw`bg-cm-black text-center justify-center flex-1`}>
      <View style={tw `p-5 bg-cm-black`}>
        <Banner/>        
      </View>
      <Text style={tw`text-white text-center text-3xl mt-15 font-genos-regular`}>
        {`The Best Tournament APP`}
      </Text>
      <Text style={tw`text-white text-center text-4 my-12 p-5 font-genos-regular`}>
        {`Nous travaillons sur une plateforme qui propose une infrastructure d'esport en ligne à tous les acteurs clés du secteur, notamment les joueurs et les organisateurs de jeu.`}
      </Text>
      <View style={tw`bg-cm-black flex items-center`}>
        <Link href={'/gallery'} style={tw`bg-cm-yellow p-3 text-center w-75 my-3 rounded-1`}>
          <Text style={tw`text-white font-inter-regular`}>Accéder à la galerie</Text>
        </Link>
        <Link href={'/avatar'} style={tw`bg-cm-yellow p-3 text-center w-75 my-3 rounded-1`}>
          <Text style={tw`text-white`}>Mes avatars</Text>
        </Link>
      </View>
    </SafeAreaView>
  );
}