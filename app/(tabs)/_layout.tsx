import React from "react";
import FontAwesome from "@expo/vector-icons/FontAwesome";
import { Tabs } from "expo-router";
import Colors from "@/constants/Colors";
import { useColorScheme } from "@/components/useColorScheme";
import { useClientOnlyValue } from "@/components/useClientOnlyValue";
import useAuthentication from "@/hooks/useAuthentification";
import { auth } from "@/services/firebaseConfig";

function TabBarIcon(props: {
    name: React.ComponentProps<typeof FontAwesome>["name"];
    color: string;
}) {
    return <FontAwesome size={28} style={{ marginBottom: -3 }} {...props} />;
}

export default function TabLayout() {
    const colorScheme = useColorScheme();

    return (
        <Tabs
            screenOptions={{
                tabBarActiveTintColor: Colors[colorScheme ?? "light"].tint,
                headerShown: useClientOnlyValue(false, true),
                tabBarStyle: { backgroundColor: "black" },
            }}
        >
            <Tabs.Screen
                name="index"
                options={{
                    title: "Accueil",
                    tabBarIcon: ({ color }) => (
                        <TabBarIcon name="home" color={color} />
                    ),
                    headerShown: false,
                }}
            />
            <Tabs.Screen
                name="gallery"
                options={{
                    title: "Gallerie",
                    tabBarIcon: ({ color }) => (
                        <TabBarIcon name="image" color={color} />
                    ),
                    headerShown: false,
                }}
            />
            <Tabs.Screen
                name="avatar"
                options={{
                    title: "Avatars",
                    tabBarIcon: ({ color }) => (
                        <TabBarIcon name="smile-o" color={color} />
                    ),
                    headerShown: false,
                }}
            />
            <Tabs.Screen
                name="profile"
                options={{
                    title: "Compte",
                    tabBarIcon: ({ color }) => (
                        <TabBarIcon name="users" color={color} />
                    ),
                    headerShown: false,
                }}
            />
        </Tabs>
    );
}
