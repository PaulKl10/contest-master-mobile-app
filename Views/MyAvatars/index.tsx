import { Text, SafeAreaView, View } from "react-native";
import React, { useEffect, useState } from "react";
import tw from "@/lib/tailwind";
import AvatarList from "@/components/AvatarList";
import AvatarService from "@/services/AvatarService";
import { useUserContext } from "@/contexts/userContext";
import { useFocusEffect } from "expo-router";

export default function MyAvatar({ navigation }: any) {
    const [avatars, setAvatars] = useState<any>(null);
    const userContext = useUserContext();

    const getMyAvatars = async () => {
        if (userContext.user) {
            const myAvatars = await AvatarService.getMyAvatars(
                userContext.user.id
            );
            setAvatars(myAvatars);
        }
    };

    useEffect(() => {
        getMyAvatars();
    }, [userContext]);

    useFocusEffect(
        React.useCallback(() => {
            getMyAvatars();
            return () => {};
        }, [])
    );

    return (
        <SafeAreaView style={tw`bg-cm-black h-[100%] pt-20`}>
            <Text style={tw`text-cm-yellow text-center text-8`}>
                {`Mes Avatars`}
            </Text>
            <View>
                <AvatarList avatars={avatars} />
            </View>
        </SafeAreaView>
    );
}
