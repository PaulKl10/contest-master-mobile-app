import { existsSync } from "fs";

let localConfig = {};
if (existsSync("./app.config.local.js")) {
    localConfig = require("./app.config.local.js");
}

export default {
    expo: {
        extra: {
            IP_local: localConfig?.extra?.IP_local || "adresse_IP",
        },
    },
};
