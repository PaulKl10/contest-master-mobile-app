import tw from "@/lib/tailwind";
import React from "react";
import { View, Text } from "react-native";

export default function Banner() {
    return (
        <View style={tw`relative`}>
            <View style={tw`gap-1 items-center px-5 py-1`}>
                <Text style={tw`text-cm-yellow text-7xl font-genos-regular`}>
                    CONTEST
                </Text>
                <Text
                    style={tw`mt-[-20px] text-7xl text-white font-genos-regular`}
                >
                    MASTER
                </Text>
            </View>
            <View
                style={tw`absolute border-2 border-white top-[50%] bottom-[0]`}
            ></View>
            <View
                style={tw`absolute border-2 border-white top-[0] bottom-[50%] right-[0]`}
            ></View>
            <View
                style={tw`absolute border-2 border-white left-[50%] right-[0] top-[0]`}
            ></View>
            <View
                style={tw`absolute border-2 border-white left-[0] bottom-[0] right-[50%]`}
            ></View>
        </View>
    );
}
