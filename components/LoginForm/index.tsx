import tw from "@/lib/tailwind";
import { useRouter } from "expo-router";
import React, { useState } from "react";
import { View, Text, TextInput, Pressable } from "react-native";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "@/services/firebaseConfig";

export default function LoginForm() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const nav = useRouter();

    const loginAndGoToHome = async () => {
        if (email.length > 0 && password.length > 0) {
            signInWithEmailAndPassword(auth, email, password)
                .then((userCredential) => {
                    // Signed in
                    const user = userCredential.user;
                })
                .then(() => {
                    alert("account loged successfully 🎉");
                    nav.navigate("/(tabs)");
                })
                .catch((err: any) => {
                    alert(err.message);
                });
        } else {
            console.log("error");
        }
    };

    return (
        <View style={tw` items-center`}>
            <TextInput
                style={tw`w-80 h-[40px] rounded border-cm-yellow border mb-[20px] pl-3 text-white`}
                placeholder="Email"
                placeholderTextColor="white"
                onChangeText={(text) => setEmail(text)}
                value={email}
                keyboardType="email-address"
            />
            <TextInput
                style={tw`w-80 h-[40px] rounded border-cm-yellow border mb-[20px] pl-3 text-white`}
                placeholder="Mot de passe"
                placeholderTextColor="white"
                onChangeText={(text) => setPassword(text)}
                value={password}
                secureTextEntry={true}
            />
            <Pressable onPress={loginAndGoToHome}>
                {({ pressed }) => (
                    <Text
                        style={
                            pressed
                                ? tw`text-gray-500 text-2xl`
                                : tw`text-cm-yellow text-2xl`
                        }
                    >
                        Se connecter
                    </Text>
                )}
            </Pressable>
        </View>
    );
}
