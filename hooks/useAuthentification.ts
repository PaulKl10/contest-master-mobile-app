import { useState, useEffect } from "react";
import { Auth, onAuthStateChanged, User } from "firebase/auth";
import { useRouter } from "expo-router";

const useAuthentication = (auth: Auth) => {
    const [currentUser, setCurrentUser] = useState<User | null>(null);
    const navigation = useRouter();

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                setCurrentUser(user);
            } else {
                navigation.navigate("/");
            }
        });

        return () => unsubscribe();
    }, [auth, navigation]);

    return currentUser;
};

export default useAuthentication;
