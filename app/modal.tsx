import { Text, View } from "@/components/Themed";
import React, { useState } from "react";
import tw from "@/lib/tailwind";

export default function ModalScreen() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    return (
        <View style={tw`flex-1 justify-center items-center bg-black`}>
            <Text style={tw`text-2xl mb-5 text-white`}>Modal</Text>
        </View>
    );
}
