import { StyleSheet, Image, Pressable } from "react-native";
import { Text, View } from "@/components/Themed";
import useGetAvatars from "@/hooks/useGetAvatars";
import { useEffect, useState } from "react";
import tw from "@/lib/tailwind";
import { SafeAreaView } from "react-native-safe-area-context";
import { TAvatar } from "@/types/avatar.types";
import { useUserContext } from "@/contexts/userContext";
import AvatarService from "@/services/AvatarService";

export default function Gallery() {
    const [avatars, setAvatars] = useState<TAvatar[]>([]);
    const fetchAvatars = useGetAvatars();
    const userContext = useUserContext();

    const handleClickAvatar = async (avatarId: number) => {
        if (!userContext.user) return;
        try {
            await AvatarService.addAvatarToUser(userContext.user.id, avatarId);
            alert("Avatar added successfully!");
        } catch (error) {
            console.error("Error adding avatar to user:", error);
        }
    };

    useEffect(() => {
        fetchAvatars
            .then((data) => {
                setAvatars(data);
            })
            .catch((error) => {
                console.error("Error fetching avatars:", error);
            });
    }, []);

    return (
        <SafeAreaView style={tw`bg-cm-black text-center justify-center flex-1`}>
            <View
                style={tw`bg-cm-black flex-1 flex-col items-center justify-center`}
            >
                <Text style={tw`text-white text-3xl py-4`}>GALLERY</Text>
                <View style={tw`bg-cm-black flex flex-row gap-3`}>
                    {avatars &&
                        avatars.map((avatar: TAvatar) => (
                            <View
                                key={avatar.id}
                                style={tw`flex flex-col items-center border border-black rounded p-4`}
                            >
                                <Image
                                    source={{
                                        uri: avatar.image,
                                    }}
                                    width={62}
                                    height={62}
                                />
                                <Text>prix: {avatar.cost}</Text>
                                <Pressable
                                    style={tw`bg-cm-yellow p-2 rounded mt-2`}
                                    onPress={() => handleClickAvatar(avatar.id)}
                                >
                                    <Text>Acheter</Text>
                                </Pressable>
                            </View>
                        ))}
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 30,
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: "80%",
    },
});
