module.exports = {
  content: [
    './app/**/*.{html,js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        'cm-yellow': '#FEB521',
        'cm-black': '#171115',
        'cm-black-footer': '#171513',
        'cm-red': '#84101D',
        'cm-gray': '#A7A6A6',
      },
      fontFamily:{
        'genos-regular': ['Genos-Regular', 'sans-serif'],
        'inter-regular': ['Inter-Regular', 'sans-serif'],
      }
    },
  },
  plugins: [],
}