export type TUser = {
    id: number;
    name: string;
    pseudo: string;
    password: string;
    email: string;
    description?: string;
    image?: string;
    city: {
        id: number;
        name: string;
        zipcode: string;
    };
    gold?: number;
    uid: string;
};
